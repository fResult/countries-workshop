module.exports = {
  globDirectory: 'public/',
  globPatterns: ['**/*.{ico,png,svg}'],
  swDest: 'public/sw.js',
  swSrc: 'public/sw.src.js'
}
