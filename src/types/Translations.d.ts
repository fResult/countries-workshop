type LangTranslations = 'de' | 'es' | 'fr' | 'ja' | 'it' | 'br' | 'pt' | 'nl' | 'hr' | 'fa'

type Translations = {
  [key in LangTranslations]: string
}
