interface ICountry {
  name: string
  topLevelDomain: string[]
  alpha2Code: string
  alpha3Code: string
  callingCodes: string[]
  capital: string
  altSpellings: string[]
  region: string
  subregion: string
  population: number
  latlng: string[]
  demonym: string
  area: number
  gini: number
  timezones: string[]
  borders: string[]
  nativeName: string
  numericCode: string
  currencies: ICurrency[]
  languages: ILanguage[]
  translations: Translations
  flag: string
  regionalBlocs: IRegionalBloc
  cioc: string
}
