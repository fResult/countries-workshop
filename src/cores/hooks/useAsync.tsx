import { AxiosError } from 'axios'
import { useCallback, useEffect, useState } from 'react'

function useAsync<T, E = Error>(
  asyncFn: (params?: any) => Promise<any>,
  immediate: boolean = true
) {
  const [value, setValue] = useState<T | Array<T> | null>(null)
  const [error, setError] = useState<E | null>(null)
  const [pending, setPending] = useState(false)

  const execute = useCallback(
    async (params?: any) => {
      setError(null)
      setValue(null)
      setPending(true)

      try {
        const { data }: { data: Array<T> | T } = await asyncFn(params)
        setValue(data)
      } catch (err) {
          setError(err as any)
        if (err instanceof Error) {
          console.error('❌ Error', err?.message)
        }
      } finally {
        setPending(false)
      }
    },
    [asyncFn]
  )

  useEffect(() => {
    if (immediate) {
      ;(async () => await execute())()
    }
  }, [execute, immediate])

  return { execute, value, pending, error }
}

export default useAsync
